import { createStore, applyMiddleware } from 'redux'

import logger from 'redux-logger'
import promise from 'redux-promise-middleware'
import thunk from 'redux-thunk'
import reducer from '../reducers'

import PERSISTED_STATE_KEY from '../constants/keys'

import setAuthorizationToken from '../utils/setAuthorizationToken'

let persistedState = {}
let defaultState = {
  menuInterface: {
    arr:[]
  },
  textResponse: {
    arr:[]
  }
}

if (localStorage.getItem(PERSISTED_STATE_KEY)){
  persistedState = JSON.parse(localStorage.getItem(PERSISTED_STATE_KEY)) 
} else {
  persistedState = defaultState
}

const middleware = applyMiddleware(promise(), thunk, logger())

if (persistedState!==defaultState){
  setAuthorizationToken(persistedState.auth.access_token)
}

const store = createStore(
  reducer, 
  persistedState,
  middleware
)

store.subscribe(()=>{
  localStorage.setItem(PERSISTED_STATE_KEY, JSON.stringify(store.getState()))
}) 

export default store
