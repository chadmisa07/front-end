import PERSISTED_STATE_KEY from '../constants/keys'

export default function() {
	localStorage.removeItem(PERSISTED_STATE_KEY)
}