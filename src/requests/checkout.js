import axios from 'axios'
import { toastr } from 'react-redux-toastr'

export function checkout(obj){

	return function(dispatch)
	{
		dispatch({type: "CHECKOUT_REQUEST" })
		axios.post("https://cbot-api.herokuapp.com/api/quick-reply/", obj)
		.then(function (response) {
			dispatch({type: "CHECKOUT_REQUEST_FULFILLED", payload: response.data})
			toastr.success('Success', 'Checkout flow successfully saved')
		})
		.catch(function (error) {
			dispatch({type: "CHECKOUT_REQUEST_REJECTED", payload: error})
			toastr.error('Error', 'Failed to save Checkout flow. Please try again.')
		});
	}
}


