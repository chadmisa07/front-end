import React from 'react'
import Header from '../../../../commons/header/Header'
import BotFlowEditorSideBar from '../../../../commons/sidebar/BotFlowEditorSideBar'
import MenuInterface from './MenuInterface'
import BotNav from '../BotNav'
import Carousel from 'nuka-carousel'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { Grid, Row, Col, ButtonGroup, Button } from 'react-bootstrap'
import { FormControl, Modal, ControlLabel, FormGroup } from 'react-bootstrap'
import { toastr } from 'react-redux-toastr'
import { createTextResponse } from '../../../../../requests/textResponse'
import { createProduct } from '../../../../../requests/product'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    menuInterface: state.menuInterface
  }
}

let count = 0

class OrderMenuContainer extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      addMode: true,
      showModal: false,
      name: "",
      code: "",
      description: "",
      price: 0,
      image_url: "",
      item_url: "",
      add_button: "",
      view_description: "",
      current_index: "",
      welcomeText: {
        "message": "",
        "text_type": 27,
        "created_by": props.user.id,
        "updated_by": props.user.id,
      },
      menuInterfaces: [this.newMenuInterface()]
    }
  }

  handleNameChange = (event) => {
    this.setState({ name: event.target.value })
  }

  handleCodeChange = (event) => {
    this.setState({ code: event.target.value })
  }

  handleDescriptionChange = (event) => {
    this.setState({ description: event.target.value })
  }

  handlePriceChange = (event) => {
    this.setState({ price: event.target.value })
  }

  handleImageChange = (event) => {
    this.setState({ image_url: event.target.value })
  }

  handleItemChange = (event) => {
    this.setState({ item_url: event.target.value })
  }

  handleAddBtnChange = (event) => {
    this.setState({ add_button: event.target.value })
  }

  handleViewDescriptionChange = (event) => {
    this.setState({ view_description: event.target.value })
  }

  newMenuInterface(){
    const userId = this.props.user.id
    return {
      "product": {
        "name": "Add Menu",
        "code": "",
        "description": "",
        "price": 0,
        "image_url": "",
        "item_url": "",
        "created_by": userId,
        "updated_by": userId
      },
      "add_button": "",
      "view_description": "",
      "index": count,
      "created_by": userId,
      "updated_by": userId
    }
  }

  open() {
     this.setState({ showModal: true })
  }

  close() {
    this.setState({ showModal: false })
  }

  addMenuInterface() {
    if (count < 10) {
      let input = this.getUserInput()
      count++
      let menuInterfaces = this.state.menuInterfaces
      menuInterfaces.unshift(input)
      this.setState({ menuInterfaces: menuInterfaces })
      this.close()
      toastr.success('Success', 'Successfully added interface')
    } else {
      toastr.info('Alert', 'Maximum of 10 interfaces reached')
    }
  }

  saveMenuInterface() {
    let menuInterfaces = this.state.menuInterfaces
    menuInterfaces[this.state.current_index] = this.getUserInput()
    this.setState({ menuInterfaces: menuInterfaces })
    this.close()
    toastr.success('Success', 'Successfully updated interface')
  }

  deleteInterface() {
    let menuInterfaces = this.state.menuInterfaces
    menuInterfaces.splice(this.state.current_index, 1)
    this.setState({ menuInterfaces: menuInterfaces })
    this.setState({ addMode: true })
    count--
    this.close()
    toastr.success('Success', 'Successfully deleted interface')
  }

  goToNextPage(){
    browserHistory.push('/bot/order_count')
  }
  
  handleWelcomeTextChange = (event) => {
    const userId = this.props.user.id
    this.setState({
      welcomeText:{
        message: event.target.value,
        "text_type": 27,
        "created_by": userId,
        "updated_by": userId
      }
    })
  }

  openAddModal(menuInterface) { 
    if (count < 10) {
      this.setState({addMode:true})
      this.mapItem(menuInterface)
      this.open()
    } else {
      toastr.info('Alert', 'Maximum of 10 interfaces reached')
    }
  }

  openEditModal(menuInterface) {
    this.setState({addMode:false, current_index:menuInterface.index})
    this.mapItem(menuInterface)
    this.open()
  }

  mapItem(menuInterface){
    this.setState({
      name: menuInterface.product.name,
      code: menuInterface.product.code,
      description: menuInterface.product.description,
      price: menuInterface.product.price,
      image_url: menuInterface.product.image_url,
      item_url: menuInterface.product.item_url,
      add_button: menuInterface.add_button,
      view_description: menuInterface.view_description,
    })
  }

  getUserInput() {
    const userId = this.props.user.id
    return {
      "add_button": this.state.add_button,
      "view_description": this.state.view_description,
      "created_by": userId,
      "updated_by": userId,
      "index": count,
      "product": {
        "name": this.state.name,
        "code": this.state.code,
        "description": this.state.description,
        "price": this.state.price,
        "image_url": this.state.image_url,
        "item_url": this.state.item_url,
        "created_by": userId,
        "updated_by": userId
      }
    }
  }

  save() {
    this.props.dispatch(createTextResponse(this.state.welcomeText))
    for(let i = 0; i < this.state.menuInterfaces.length; i++){
      if (i!==this.state.menuInterfaces.length - 1){
        this.props.dispatch(createProduct(this.state.menuInterfaces[i]))
      }
    }
    this.goToNextPage()
  }

  render() {

    const menus = this.state.menuInterfaces.map((menuInterface, index) => {
      let callback
      if (count===0 ||index===count) {
        callback = () => this.openAddModal(menuInterface)
      } else {
        callback = () => this.openEditModal(menuInterface)
      }
      menuInterface.index = index
      return (
        <div className="text-center" key={index}>
          <ButtonGroup justified>
            <Button href="#" key={index} bsStyle="info" className="blue-text"
              onClick={callback} disabled={index===10}>
                {menuInterface.product.name}
            </Button>
          </ButtonGroup>
        </div>
      )
    })

    const cards = this.state.menuInterfaces.map((menuInterface, index) => {
      return (
        <MenuInterface key={index} product={menuInterface.product}
          add_button={menuInterface.add_button} 
          view_description={menuInterface.view_description} />
      )
    }) 


    return (
      <Grid fluid>
        <Row>
          <Header titleLeft="Create Your Bot" titleRight="Logout your account "/>
        </Row>
        <Row>
          <BotFlowEditorSideBar selected="editMenu" />
          <div className="page-content">
            <BotNav bot={this.props.bot.bot} goToNextPage={this.goToNextPage} save={this.save.bind(this)} />
            <Col lg={6} className="form-padding">
              <Row>
                <Col lg={12}>
                  <h4 className="blue-text-color">Define the Menu</h4>
                </Col>
              </Row>
              <Row className="form-padding">
                <Col lg={4}>
                  <h4>1. Edit Menu</h4>
                </Col>
                <Col lg={8}>
                  <FormControl componentClass="textarea" placeholder="Enter Message"
                    value={this.state.welcomeText.message}
                    onChange={this.handleWelcomeTextChange.bind(this)} />
                  <div className="form-padding">
                    {menus}
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={6} className="form-padding">
              <div>
                <Carousel>
                  {cards}
                </Carousel>
              </div>
            </Col>
          </div>
        </Row>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Add Menu</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <FormGroup>
                <ControlLabel>Product Name</ControlLabel>
                <FormControl type="text" placeholder="Enter text" ref="name" 
                  value={this.state.name}
                  onChange={this.handleNameChange} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Code</ControlLabel>
                <FormControl type="text" placeholder="Enter text" ref="code" 
                  value={this.state.code}
                  onChange={this.handleCodeChange} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Product Description</ControlLabel>
                <FormControl type="textarea" placeholder="Enter text" ref="description"
                  value={this.state.description}
                  onChange={this.handleDescriptionChange} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Product URL</ControlLabel>
                <FormControl type="text" placeholder="Product URL" ref="item_url"
                  value={this.state.item_url}
                  onChange={this.handleItemChange}  />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Image URL</ControlLabel>
                <FormControl type="text" placeholder="Image URL" ref="image_url"
                  value={this.state.image_url}
                  onChange={this.handleImageChange} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Price</ControlLabel>
                <FormControl type="number" ref="price"
                  value={this.state.price}
                  onChange={this.handlePriceChange} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Add to Cart Button Title</ControlLabel>
                <FormControl type="text" placeholder="Button Title 1" ref="add_button" 
                  value={this.state.add_button}
                  onChange={this.handleAddBtnChange} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>View Description Button Title</ControlLabel>
                <FormControl type="text" placeholder="Button Title 2" ref="view_description" 
                  value={this.state.view_description}
                  onChange={this.handleViewDescriptionChange} />
              </FormGroup>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button className="blue-text" 
              onClick={this.state.addMode? this.addMenuInterface.bind(this): 
                this.saveMenuInterface.bind(this)}>
                  {this.state.addMode? "Add":"Update"}
            </Button>
            {!this.state.addMode? 
              <Button onClick={this.deleteInterface.bind(this)} bsStyle="danger">Delete</Button>
              :
              null
            }
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(OrderMenuContainer)
