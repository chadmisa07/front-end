import React from 'react'
import { Col, Table, ButtonGroup, Button } from 'react-bootstrap'

class BotNav extends React.Component{
  render() {
    return (
      <Col lg={12}>
        <Table bordered className="form-padding half-width">
          <tbody>
            <tr>
              <td><b>Job Name</b></td>
              <td>Order 001</td>
              <td colSpan="2" className="save-next-btn">
                <ButtonGroup justified>
                  <Button href="#" bsSize="xsmall" className="blue-text"
                    onClick={this.props.save}>
                      Save
                  </Button>
                  <Button href="#" bsSize="xsmall" className="blue-text" 
                    onClick={this.props.goToNextPage.bind(this)}>
                      Next
                  </Button>
                </ButtonGroup> 
              </td>
            </tr>
            <tr>
              <th>Associated Bot</th>
              <td colSpan="3">{this.props.bot.id}</td>
            </tr>
          </tbody>
        </Table>
      </Col>
    )
  }
}

export default BotNav
  