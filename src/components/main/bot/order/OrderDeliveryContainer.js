import React from 'react'
import Header from '../../../commons/header/Header'
import BotFlowEditorSideBar from '../../../commons/sidebar/BotFlowEditorSideBar'
import { toastr } from 'react-redux-toastr'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { Grid, Row, Col, Table, ButtonGroup, Button, Checkbox, ControlLabel, FormGroup, Form } from 'react-bootstrap'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  }
}

class OrderDeliveryContainer extends React.Component{

  constructor(props) {
    super(props)
    this.state = {delivery:false}
  }

  gotoNextPage() {
    browserHistory.push('/bot/checkout')
  }

  handleChange = (event) => {
    this.setState({ delivery: !this.state.delivery })
  }

  save() {
    this.props.dispatch({type: "DELIVERY_OPTION_CHOSEN", payload: this.state.delivery})
    this.gotoNextPage()
    toastr.success('Success', "Delivery option saved")
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Header titleLeft="Create Your Bot" titleRight="Logout your account "/>
        </Row>
        <Row>
          <BotFlowEditorSideBar selected="editDelivery" />
          <div className="page-content">
            <Col lg={12}>
              <Table bordered className="form-padding half-width">
                <tbody>
                  <tr>
                    <td><b>Job Name</b></td>
                    <td>Order 001</td>
                    <td colSpan="2" className="save-next-btn">
                      <ButtonGroup justified>
                        <Button href="#" bsSize="xsmall" className="blue-text" onClick={this.save.bind(this)}>Save</Button>
                        <Button href="#" bsSize="xsmall" className="blue-text" onClick={this.save.bind(this)}>Next</Button>
                      </ButtonGroup> 
                    </td>
                  </tr>
                  <tr>
                    <th>Associated Bot</th>
                    <td colSpan="3">Bot-ID Here</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col lg={6} className="form-padding">
              <Row>
                <Col lg={12}>
                  <h4 className="blue-text-color">Define Delivery Options</h4>
                </Col>
              </Row>
              <Row className="form-padding">
                <Col lg={12} className="form-padding">
                  <h4>4.  Delivery  / Pickup Options</h4>
                  <Col md={12} className="form-padding">
                    <Form horizontal>
                      <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={3}>
                          Allow delivery
                        </Col>
                        <Col sm={9}>
                          <Checkbox onChange={this.handleChange.bind(this)} >
                            Yes, ask user for delivery location.
                          </Checkbox>
                        </Col>
                      </FormGroup>
                    </Form>
                  </Col>
                </Col>
              </Row>
            </Col>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(OrderDeliveryContainer)
