import React from 'react'
import { Form, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap'

class CreateBotForm extends React.Component{
  render(){
    return (
      <Form className="form-padding">
        <FormGroup bsSize="small">
          <ControlLabel>Choose Platform</ControlLabel>
          <FormControl componentClass="select" placeholder="Platform" 
              value={this.props.platform} 
              onChange={this.props.handlePlatformChange.bind(this)} >
            <option value="3">Facebook</option>
          </FormControl>
        </FormGroup>
        <FormGroup bsSize="small">
          <ControlLabel>Enter Name</ControlLabel>
          <FormControl type="text" placeholder="Name" 
            value={this.props.name}
            onChange={this.props.handleNameChange.bind(this)} />
        </FormGroup>
        <FormGroup bsSize="small">
          <ControlLabel>Enter Description</ControlLabel>
          <FormControl componentClass="textarea" placeholder="Description" 
            value={this.props.description} 
            onChange={this.props.handleDescriptionChange.bind(this)} />
        </FormGroup>
        <Button bsStyle="primary" bsSize="small" 
          onClick={this.props.createBot}
          disabled={this.props.creating} >
            {this.props.creating ? 'Creating bot...' : 'Create Bot'}
        </Button>
      </Form>
    )
  }
}

export default CreateBotForm