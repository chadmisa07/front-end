import React from 'react'
import clearAuth from '../utils/clearAuth'
import { Grid, Row, Col } from 'react-bootstrap'
import '../includes/css/home.css'

class LogoutForm extends React.Component {
  componentDidMount(nextProps, nextState) {
    clearAuth()
  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={6} mdOffset={3} className="text-center row-padding">
            <h4>You have now successfully logged out.</h4>
            <h5>Click <a href="login">here </a>to login again.</h5> 
          </Col>
        </Row>
      </Grid>
    )
  }
}

export default LogoutForm