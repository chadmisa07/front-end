import React, { Component } from 'react'

class CreateBotSidebar extends Component{
  render(){
    return (
      <div className="left-side-bar">
        <ul className="sidebar">
          <li className="sidebar">
            <a className={this.props.selected==='createBot'? "sidebar active":"sidebar"}>Create Bots</a>
          </li>
          <li className="sidebar">
            <a className={this.props.selected==='editBot'? "sidebar active":"sidebar"}>Edit Bots</a>
          </li>
          <li className="sidebar">
            <a className={this.props.selected==='editBotJobs'? "sidebar active":"sidebar"}>Edit Bot Jobs</a>
          </li>
        </ul>
      </div>
    )
  }
}

export default CreateBotSidebar