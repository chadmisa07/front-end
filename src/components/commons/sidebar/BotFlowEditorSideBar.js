 import React, { Component } from 'react'

class BotFlowSideBar extends Component{
  render(){
    return (
      <div className="flow-left-side-bar">
        <ul className="sidebar">
          <li className="sidebar">
            <a className="sidebar-header">Bot Flow Editor</a>
          </li>
          <li className="sidebar">
            <a className="sidebar-text text-center">Here you can edit your bots</a>
          </li>
          <li className="sidebar">
            <a className="sidebar">Order Bot</a>
          </li>
          <li className="sidebar">
            <a className={this.props.selected==='editMenu'? "sidebar active":"sidebar"}>1. Menu</a>
          </li>
          <li className="sidebar">
            <a className={this.props.selected==='orderCount'? "sidebar active":"sidebar"}>2. Order Count</a>
          </li>
          <li className="sidebar">
            <a className={this.props.selected==='editCart'? "sidebar active":"sidebar"}>3. Cart</a>
          </li>
          <li className="sidebar">
            <a className={this.props.selected==='editDelivery'? "sidebar active":"sidebar"}>4. Delivery / Pickup</a>
          </li>
          <li className="sidebar">
            <a className={this.props.selected==='editCheckout'? "sidebar active":"sidebar"}>5. Checkout</a>
          </li>
        </ul>
      </div>
    )
  }
}

export default BotFlowSideBar