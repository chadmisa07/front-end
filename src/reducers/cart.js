export default function reducer(state={
	buttons: [],
	buttonId: null,
	responseText: null, 
	responseTextID: null,
	saving: false,
	fetching: false,
	success: false,
	failed: false,
	nextButtonDisabled: true,
	error: null,
}, action){

	switch(action.type)
	{
		case "CART_BUTTON_SAVE_REQUEST": {
			return {...state, saving: true}
		}
		case "CART_BUTTON_SAVE_REQUEST_FULFILLED":{
			// return {...state, saving: false, success: true, image_url: action.image_url, title: action.title, description: action.description, button_text: action.button_text}
			return {...state, saving: false, nextButtonDisabled: false, success: true, buttons:action.payload.buttons, buttonId:action.payload.id }
		}
		case "CART_BUTTON_SAVE_REQUEST_REJECTED":{
			return {...state, saving: false, failed: true ,error: action.payload}
		}

		case "TEXT_RESPONSE_SAVE_REQUEST": {
			return {...state, saving: true}
		}
		case "TEXT_RESPONSE_SAVE_REQUEST_FULFILLED":{
			// return {...state, saving: false, success: true, image_url: action.image_url, title: action.title, description: action.description, button_text: action.button_text}
			return {...state, saving: false, nextButtonDisabled: false, success: true, responseText: action.payload.message, responseTextID: action.payload.id}
		}
		case "TEXT_RESPONSE_SAVE_REQUEST_REJECTED":{
			return {...state, saving: false, failed: true ,error: action.payload}
		}

		default: {
			return state;
		}
	}
	
}