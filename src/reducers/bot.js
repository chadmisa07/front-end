const default_state = {
  platform: null,
  name: null,
  description: null,
  creating: false,
  success: false,
  failed: false,
  error: null,
}

export default function reducer(state=default_state, action){
  switch(action.type) {
    case "CREATE_BOT_REQUEST": {
      return {...state, creating: true}
    }
    case "CREATE_BOT_REQUEST_FULFILLED":{
      return {...state, creating: false, success: true, bot: action.payload}
    }
    case "CREATE_BOT_REQUEST_REJECTED":{
      return {...state, creating: false, failed: true, error: action.payload}
    }
    default: {
      return state;
    }
  }
  
}