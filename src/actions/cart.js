import { saveCartRequest } from '../requests/cart'

export function saveCart(textResponseObj, buttonsObj){
	return saveCartRequest(textResponseObj, buttonsObj)
}